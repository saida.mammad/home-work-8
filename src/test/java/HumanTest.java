import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class HumanTest {
    private Man human = new Man("Saida", "Salimova", 1983, 100, null);
    private Woman human1 = new Woman("Mammad", "Salimov", 1987, 100, null);

    @Before
    public String getExpectedToString(Human human) {
        String result = "Human{name=" + human.getName() +
                ", surname=" + human.getSurname() +
                ", year=" + human.getYear() +
                ", iq=" + human.getIq() +
                ", weekNotes=" + human.getWeekNotes() + "}";
        return result;
    }

    @Test
    public void testToString() {
        Assert.assertEquals(human.toString(), getExpectedToString(human));
    }
}