import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

public class FamilyTest {
    private Human mother1 = new Woman("Saida", "Salimova");
    private Human father1 = new Man("Mammad", "Salimov");

    private Family family1 = new Family(mother1, father1);
    private Human child1 = new Woman("Nazrin", "Salimli");
    private Human child2 = new Man("Kamil", "Salimov");

    private Human mother2 = new Woman("Sitara", "Mammadova");
    private Human father2 = new Man("Farman", "Mammadov");
    private Family family2 = new Family(mother2, father2);

    @Before
    public void setUp() {
        family1.addChild(child1);
        family1.addChild(child2);
    }

    public String getExpectedToString(Family family) {
        String result = "Family{mother=" + family.getMother() +
                ", father=" + family.getFather() +
                ", children=" + family.getChildren() + ", pet=null}";
        return result;
    }

    @Test
    public void testToString() {
        Assert.assertEquals(family1.toString(), getExpectedToString(family1));
    }

    @Test
    public void testDeleteChildPositive() {
        int resultBefore = family1.getChildren().size();
        family1.deleteChild(child2);
        Assert.assertEquals(family1.getChildren().size(), resultBefore - 1);

        for (Human child : family1.getChildren()) Assert.assertNotSame(child, child2);
    }

    @Test
    public void testDeleteChildNegative() {
        List<Human> resultBefore = family1.getChildren();
        family1.deleteChild(new Woman("Nazrin", "Salimli"));
        Assert.assertEquals(resultBefore, family1.getChildren());
    }

    @Test
    public void testDeleteChildIndexPositive() {
        int resultBefore = family1.getChildren().size();
        family1.deleteChild(1);
        assertEquals(1, resultBefore - 1);
        assertSame(family1.getChildren().get(0), child1);
    }

    @Test
    public void testDeleteChildIndexNegative() {
        assertEquals(2, family1.getChildren().size());
        assertSame(family1.getChildren().get(0), child1);
    }

    @Test
    public void testAddChildPositive() {
        int resultBefore = family1.getChildren().size();
        Human newChild = new Man("Leyli", "Salimli");
        family1.addChild(newChild);
        Assert.assertEquals(family1.getChildren().size(), resultBefore + 1);
        Assert.assertEquals(family1.getChildren().get(family1.getChildren().size() - 1), newChild);

    }

    @Test
    public void testCountFamily() {
        int resultCount = family2.countFamily();
        family2.addChild(new Man());
        Assert.assertEquals(++resultCount, family2.countFamily());
        family2.addChild(new Man());
        Assert.assertEquals(++resultCount, family2.countFamily());
    }
}
