import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Main {
    public static void main(String[] args) {


        Woman human1 = new Woman("Saida", "Salimova");
        Man human2 = new Man("Mammad", "Salimov");


        Map<String, String> string1 = new HashMap<>();
        string1.put(DayOfWeek.SUNDAY.name(), "Meet with friends");
        string1.put(DayOfWeek.MONDAY.name(), "Go to slip");

        human1.setWeekNotes(string1);
        System.out.println(human1);

        Human mother = new Woman();
        Human father = new Man();
        Family family = new Family(mother, father);

        Human son = new Man("Kamil", "Salimov");

        family.addChild(son);
        System.out.println(son.getFamily());

        family.deleteChild(son);
        System.out.println(son.getFamily());

        Set<String> strings2 = new HashSet<>();
        strings2.add("Делаю что хочу");

        Dog dog = new Dog("Vafle", 5, 75, strings2);

        dog.habits.add("Привет хозяин");
        System.out.println(dog.habits);
        System.out.println(dog);


        Cat cat = new Cat("Snow", 2, 80, strings2);

        Set<Pet> pets = new HashSet<>();
        pets.add(dog);
        pets.add(cat);
        family.setPet(pets);
        System.out.println(family);

    }
}

