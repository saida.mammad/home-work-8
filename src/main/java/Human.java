
import java.util.Map;
import java.util.Objects;

abstract class Human {
    private String name;
    private String surname;
    private int year;
    private int iq;
    private Map<String, String> weekNotes;
    private Family family;

    public Human() {
    }

    public Human(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    public Human(String name, String surname, int year, int iq, Map<String, String> weekNotes) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.weekNotes = weekNotes;
    }

    public abstract void greetPet(Pet pet);

    public void describePet(Pet pet) {
        System.out.println("У меня есть " + pet.getSpecies() + ". Eму " + pet.getAge() + " лет/года, он хитрый!");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return year == human.year &&
                Objects.equals(name, human.name) &&
                Objects.equals(surname, human.surname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, year);
    }

    @Override
    public String toString() {
        return "Human{name=" + name + ", surname=" + surname + ", year=" + year +
                ", iq=" + iq + ", weekNotes=" + weekNotes + "}";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getYear() {
        return year;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public int getIq() {
        return iq;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public void setWeekNotes(Map<String, String> weekNotes) {
        this.weekNotes = weekNotes;
    }

    public Map<String, String> getWeekNotes() {
        return weekNotes;
    }
}


